import { Component } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  genders: any = []; // Array<string>; // string[]; // [];
  times: any = [];
  bottles: any = [];
  weight: number;
  gender: string;
  bottle: number;
  promilles: number;
  litres: number;
  grams: number;
  burn: number;
  gramsleft: number;
  time: number;

  constructor() {}

  ngOnInit() {
    this.genders.push('Male');
    this.genders.push('Female');
  
    this.times.push(1);
    this.times.push(2);
    this.times.push(3);
    this.times.push(4);
    this.times.push(5);
    this.times.push(6);
    this.times.push(7);
    this.times.push(8);

    this.bottles.push(1);
    this.bottles.push(2);
    this.bottles.push(3);
    this.bottles.push(4);
    this.bottles.push(5);
    this.bottles.push(6);
    this.bottles.push(7);
    this.bottles.push(8);
    this.bottles.push(9);
    this.bottles.push(10);
  
    this.gender = 'Male';
    this.time = 1;
    this.bottle = 1;

  }

  calculate() {

    this.litres = this.bottle * 0.33;
    this.grams = this.litres * 8 * 4.5;
    this.burn = this.weight / 10;
    this.gramsleft = this.grams - (this.burn * this.time);
    
    if (this.gender === 'Male') {
      this.promilles = this.gramsleft / (this.weight * 0.7);
      //= (factor * 0.33) * 8 * 4.5;
    } else {
      this.promilles = this.gramsleft / (this.weight * 0.6);
    }
  }

}
